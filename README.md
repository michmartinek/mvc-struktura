#MVC Struktura

Jednoduchý projekt v PHP, myšlený jako úvod do MVC.
 
samotná aplikace se dělí na 3 části kontrolery, modely a šablony a je uložena ve složce app.


Kontrolery zpracovávájí requesty, posílají data dále do modelů, které tyto data zpracují, vrátí zpět výsledek, který opět pomocí kontroleru míři do šablony.

Router zpracovává požadavek na server tímhle způsobem:

1. za adresou není nic, třeba domena.com, spustí DefaultController a v ném funkci index.

2. za adresou je pouze jedna věc domena.com/User, spustí kontroler UserController a v něm funkci index

3. za adresou jsou 2 a více věcí oddělená pomocí / třeba domena.com/User/signin/jmeno, spustí UserController, v něm funkci siginin a té předá jako paramter řetězec "jmeno"




Věci jako js, css a obrázky patří do složky public.


##Git - příkazy
git clone <url> - stáhne k vám repozitář

git remote add origin <url> - přidá server, aby jste na něj mohli ukládat, <url> bude něco jako https://username@bitbucket.org/michmartinek/mvc-struktura.git

###provedťe nějaké změny, třeba dopište něco v nějaké šabloně, a poté už stačí dělat tohle:

git add * - přidá všechny změněné soubory ke commitu

git commit -m "text ke provedeným změnám" - commitne provedené změny

git push origin master - převede zmeny na server, bude po vás chtít heslo
